# yunxiangshop

## Project setup
```
npm install

vue运行环境部署参考一下文档：
https://blog.csdn.net/qq_63586399/article/details/135576997?ops_request_misc=&request_id=&biz_id=102&utm_term=vue%20ui%E5%88%9B%E5%BB%BA%E9%A1%B9%E7%9B%AE&utm_medium=distribute.pc_search_result.none-task-blog-2~all~sobaiduweb~default-0-135576997.nonecase&spm=1018.2226.3001.4187

需要指出，在你下载之后，无需创建新的项目，只需要在vue ui页面打开管理端项目。

如果在cmd面板遇到：‘ vue‘ 不是内部或外部命令，也不是可运行的程序或批处理文件。
可参考以下文档解决：
https://blog.csdn.net/weixin_44566194/article/details/129962361?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522171387667716800226521346%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=171387667716800226521346&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~top_positive~default-1-129962361-null-null.142^v100^pc_search_result_base3&utm_term=vue%20%E4%B8%8D%E6%98%AF%E5%86%85%E9%83%A8%E6%88%96%E5%A4%96%E9%83%A8%E5%91%BD%E4%BB%A4%EF%BC%8C%E4%B9%9F%E4%B8%8D%E6%98%AF%E5%8F%AF%E8%BF%90%E8%A1%8C%E7%9A%84%E7%A8%8B%E5%BA%8F%20%E6%88%96%E6%89%B9%E5%A4%84%E7%90%86%E6%96%87%E4%BB%B6%E3%80%82&spm=1018.2226.3001.4187
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
