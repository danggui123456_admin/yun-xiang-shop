# YunXiangShop

#### 介绍
本项目为软件工程项目大作业。

#### 软件架构
本项目采用主流技术栈实现，Mysql数据库，SpringBoot2+Mybatis Plus后端，微信小程序原生实现，Vue3.2+Element Plus实现后台管理。基于JWT技术实现前后端分离。

#### 使用说明

1.  小程序appid为：wx3167cda3e7823fd3
2.  各位开发者在开发的过程中，使用具体技术的版本号需要一致（maven、springboot等）
3.  项目部署路径尽量全英文，不要有中文
4.  遇到问题，随时通过即时通讯工具进行交流
5.  提交代码的时候，需要提交到自己创建的分支上
6.  未完待续…………

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
5.  希望各位在开发过程中有所收获(smile)(smile)(smile)


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
